from g4epy import Geant4Eic
g4e = Geant4Eic(detector='jleic', beamline='jleic')\
        .command(['/lmon/construct/collim 1',
                  '/lmon/construct/magnet 1',
                  '/lmon/construct/ewV2 1',
                  '/lmon/construct/phot 1',
                  '/lmon/construct/up 1',
                  '/lmon/construct/down 1'])\
         .source('../data/herwig6_20k.hepmc')\
         .output('test_full')\
         .beam_on(1)\
         .vis()\
         .run()