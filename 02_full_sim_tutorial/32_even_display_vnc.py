from g4epy import Geant4Eic

g4e = Geant4Eic(detector='jleic', beamline='erhic')\
         .source('../data/herwig6_20k.hepmc')\
         .output('jleic')\
         .beam_on(1)\
         .vis()      # <== Start with initialization mode

g4e.run()