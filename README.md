# epw

EIC Entry Point Workspace. Examples and documentation to get started with EIC software


## Run using docker

```bash
docker run --rm -it -p 8888:8888 eicdev/epic:latest
```

```--rm``` flag Docker **automatically cleans up the container** and remove the file system **when 
the container exits**. By default (without --rm flag) a container’s file system persists 
even after the container exits. This makes debugging easier and you retain all your data. 
[More on the docker docs](https://docs.docker.com/engine/reference/run/#clean-up---rm). 
We use --rm flag for the sake of the tutorial repeatability. **If you work with the container, 
might be, you don't need it**. 


```-it``` flag enables interactive session. Without this flag ctrl+c will not work on Mac 
machines. In general `-it` is used to run e.g. bash session (see below)


## Explore more

- [Jupyter community guides](https://jupyter.readthedocs.io/en/latest/community/content-community.html)
- [Python Data Science Handbook](https://github.com/jakevdp/PythonDataScienceHandbook)

### Run without jupyter lab

You can start the docker without Jupyter Lab running:

```bash
docker run --rm -it -p 8888:8888 eicdev/epic:latest bash
```

Later you can still run jupyter lab by 

```bash
jlab    # It is Jupyter Lab here, not Jefferson
```

### Changing and saving the contents

The files that you see in Jupyter Lab are stored in Git repository: 
 
https://gitlab.com/eic/epw

So when you do the change, that is important to save, you could make 
you branch or fork the repo. This  

 


### Troubleshooting
If docker gives an error like this:
> Error starting userland proxy: listen tcp 0.0.0.0:8888: bind: address already in use.

It usually means, that the port 8888 is used by another application. 
To fix that try to change `-p 8888:8888` flag to `-p <something>:8888` 
e.g. `-p 9999:8888`. Put the same port in your browser:
```
127.0.0.1:9999/lab
```

## Saving git

Open terminal

```bash
git config --global user.name "Your Name"
git config --global user.email "your@email"
git config --global push.default simple

# Now you can commit and push changes  like: changes 
git status
git add how_to_git.ipynb
git commit -m "how to git added"

#Create a new branch:
git branch newfeature

# Checkout new branch: (this will not reset your work.)
git checkout newfeature

# Now commit your work on this new branch:
git commit -s

# Push your branch to server
git push -u origin feature_branch_name
```

More on working with git: [Resources to learn Git](https://try.github.io/)


# This will save your credential for 1 hour
!git config --global credential.helper 'cache --timeout=3600'


