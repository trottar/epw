
#include "fastjet/ClusterSequence.hh"
//#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "TSystem.h"

#include "eicsmear/erhic/EventBase.h"
#include "eicsmear/erhic/EventPythia.h"
#include "eicsmear/erhic/Particle.h"
#include "eicsmear/smear/EventSmear.h"
#include "eicsmear/smear/EventS.h"
#include "eicsmear/smear/Smear.h"
#include "eicsmear/smear/ParticleMCS.h"

using namespace fastjet;
using namespace std;

int main(int argc, char* argv[]) {

  TString inFileName1 = "smallpythia.root";
  TString inFileName2 = "smearedsmall.root";
  // TString inFileName1 = "largepythia.root";
  // TString inFileName2 = "smearedlarge.root";

  TString outname = inFileName1;
  outname.Prepend( "analyzed_");

  // Chain for Simu Tree
  gSystem->Load("libeicsmear");

  TChain* inTree = new TChain("EICTree");
  inTree->Add(inFileName1);
  inTree->AddFriend("Smeared",inFileName2);

  // Setup Input Event Buffer
  erhic::EventPythia* inEvent(NULL);    inTree->SetBranchAddress("event",&inEvent);
  Smear::Event* inEventS(NULL);         inTree->SetBranchAddress("eventS",&inEventS);

  double R=0.5;
  JetDefinition jet_def = JetDefinition(kt_algorithm, R);

  const double beta_sd =  0.0; // 1.0 is interesting
  const double z_cut = 0.1;
  contrib::SoftDrop sd( beta_sd, z_cut );
  

  // CAREFUL! These ranges are not yet used below
  double etaMax = 4.0;
  double etaMin = -etaMax;
  double jetPtCut=5.0;
  auto select_jet_eta     = fastjet::SelectorEtaRange ( etaMin + R, etaMax -R  );
  auto select_jet_pt      = SelectorPtRange( jetPtCut, 1000 );
  auto select_jet         = select_jet_eta * select_jet_pt;     

  TFile* fout = new TFile ( outname, "RECREATE");

  // Histograms
  // ----------
  int nptbins = 100;
  float ptmin = 0;
  float ptmax = 10; 
  // TH2D* hchargedc_pt = new TH2D ( "hchargedc_pt", "Charged Constituents;p_{T} [GeV]; arb. u.", nptbins, ptmin, ptmax, nptbins, ptmin, ptmax );
  // TH2D* hneutralc_pt = new TH2D ( "hneutralc_pt", "Neutral Constituents;p_{T} [GeV]; arb. u.", nptbins, ptmin, ptmax, nptbins, ptmin, ptmax );				  
  TH2D* hc_pt = new TH2D ( "hc_pt", "Constituents;p_{T}^{orig} [GeV]; p_{T}^{smear} [GeV]; arb. u.", nptbins, ptmin, ptmax, nptbins, ptmin, ptmax );

  int netabins = 100;
  float etamin = -5;
  float etamax = 5; 
  TH2D* hc_eta = new TH2D ( "hc_eta", "Constituents;#eta^{orig}; #eta^{smear} [GeV]; arb. u.", netabins, etamin, etamax, netabins, etamin, etamax );

  int njptbins = 40;
  float jptmin = -1;
  float jptmax = 19; 
  TH2D* hjlead_pt = new TH2D ( "hjlead_pt", "LeadingJets;p_{T}^{orig} [GeV]; p_{T}^{smear} [GeV]; arb. u.", njptbins, jptmin, jptmax, njptbins, jptmin, jptmax );

  int njmbins = 40;
  float jmmin = -1;
  float jmmax = 9; 
  TH2D* hjlead_m = new TH2D ( "hjlead_m", "LeadingJets;m^{orig} [GeV]; m^{smear} [GeV]; arb. u.", njmbins, jmmin, jmmax, njmbins, jmmin, jmmax );

  int njzgbins = 24;
  float jzgmin = -0.05;
  float jzgmax = 0.55; 
  TH2D* hjlead_zg = new TH2D ( "hjlead_zg", "LeadingJets;z_{g}^{orig} [GeV]; z_{g}^{smear} [GeV]; arb. u.", njzgbins, jzgmin, jzgmax, njzgbins, jzgmin, jzgmax );

  vector<PseudoJet> OriginalConstituents;
  vector<PseudoJet> SmearedConstituents;
  
  // Loop Over Events in Simu Trees
  long int nevents=inTree->GetEntries();
  // nevents=50000;

  for(int iEvent=0; iEvent<nevents; iEvent++){

    //Read Next Event
    if(inTree->GetEntry(iEvent) <=0) break;
    if(iEvent%10000 == 0) cout << "Event " << iEvent << endl;

    OriginalConstituents.clear();
    SmearedConstituents.clear();


    // Loop over Particles
    for(int j=0; j<inEventS->GetNTracks(); j++){

      // Skip beam
      if ( j<3 ) continue;

      const Smear::ParticleMCS* inParticleS = inEventS->GetTrack(j); // Smeared Particle      
      const Particle* inParticle = inEvent->GetTrack(j); // Unsmeared Particle
      
      Double_t px = -999.;
      Double_t py = -999.;
      Double_t pz = -999.;
      Double_t E = -999.;

      // Skip non-final particles. 
      if ( inParticle->GetStatus() != 1 ) continue;

      // Impose acceptance - at truth level because I'm lazy.
      if ( TMath::Abs(inParticle->GetEta()) > 4.0 ) continue;	

      Int_t localCode = TMath::Abs(inParticle->GetPdgCode());
      
      // Smeared?
      if(inParticleS == NULL) {// Particle was not smeared, use original
	px = inParticle->GetPx();
	py = inParticle->GetPy();
	pz = inParticle->GetPz();
	E = inParticle->GetE();
      } else  {// Particle was smeared
	Double_t tmpX = inParticleS->GetPx();
	Double_t tmpY = inParticleS->GetPy();
	Double_t tmpZ = inParticleS->GetPz();
	Double_t tmpE = inParticleS->GetE();

	//Double_t tmpOrigE = inParticle->GetE();

	// an identical 0 could mean trouble.
	// Will skip these for jet finding.
	if(TMath::Abs(tmpX) < 0.0001 || TMath::Abs(tmpY) < 0.0001 || TMath::Abs(tmpE) < 0.0001) {
	  // cout << "Event " << iEvent << " Particle " << j << " Smeared Bad" << " " << tmpX << " " << tmpY << " " << tmpZ << " " << tmpE << endl;
	  continue;
	}
	
	// Smearing affects different particles differently. Need to go case by case.

	double factor2;
	switch ( inParticle->GetPdgCode() ){
	case 11 : // e
	case 22 : // gamma
	case 2112 : // n
	case 130 : // K0L


	}
      }
	  
      if ( charged ) {
	...
      }





	  // EM particles have only their energy smeared, momentum is original
	  // Currently will treat clusters (pi0s and electrons) as massless
	  // Need to alter P so that P^2 = E^2
	  // Do for Neutral Hadrons too	  

      	  factor2 = (tmpE*tmpE)/(tmpX*tmpX + tmpY*tmpY + tmpZ*tmpZ);	  
	  px = tmpX*TMath::Sqrt(factor2);
	  py = tmpY*TMath::Sqrt(factor2);
	  pz = tmpZ*TMath::Sqrt(factor2);
	  E = tmpE;
	  break;

	default :
	  if( TMath::Abs(inParticle->GetEta()) > 3.5 &&  TMath::Abs(inParticle->GetEta()) <= 4.0 ){
	    
	    // Charged Hadrons with 3.5 < |eta| < 4 will not be smeared by the tracking system
	    // Will be detected by the hadron calorimeter
	    // Need to alter P for these events same as for neutral hadrons above
	    factor2 = (tmpE*tmpE)/(tmpX*tmpX + tmpY*tmpY + tmpZ*tmpZ);
	    px = tmpX*TMath::Sqrt(factor2);
	    py = tmpY*TMath::Sqrt(factor2);
	    pz = tmpZ*TMath::Sqrt(factor2);
	    E = tmpE;
	  } else if ( TMath::Abs(inParticle->GetEta()) <= 3.5 ){
	    
	    // Charged Hadrons have only their momentum smeared, energy is original
	    // Currently will treat tracks as having pion mass
	    // Need to alter E so that E^2 = P^2 + mp^2
	    Double_t P2 = tmpX*tmpX + tmpY*tmpY + tmpZ*tmpZ;
	    
	    px = tmpX;
	    py = tmpY;
	    pz = tmpZ;
	    E = TMath::Sqrt(P2 + 0.139570*0.139570);
	  } else {
	    // Just sanity check.
	    cout << "This should never happen. Eta = " << TMath::Abs(inParticle->GetEta()) << " for pdg " << localCode << endl;\	    
	  }
	}

      } // Smeared?

      // Select Particles for Jets
      if( inParticle->GetStatus() != 1){
	throw(-1);
      }

      if ( inParticle->GetParentIndex() == 3 ) continue; // scattered electron children
      
      if(j<=10) {
	auto process = inEvent->GetProcess();
	// Note Brian didn't accept anything below line 11
	// I'll keep them for now, seems okay in MinBias?
	// if ( process < 90 || process > 98 ){
	//   cerr << "this shouldn't happen " << " event # " << iEvent
	//        << " Particle info: " << j << "  " << localCode << "  " << inParticle->GetStatus()
	//        << "  " << inParticle->GetParentIndex()
	//        << "  " << inParticle->GetPt()

	//        << "  process " << process<< endl;
	// }
      }

      // Can treat charged and neutral differently here. For now accept all.
      // Create random number for track rejection
      // Double_t ranFactor = r.Uniform(0.0,1.0);

      OriginalConstituents.push_back( PseudoJet (inParticle->GetPx(),
						 inParticle->GetPy(),
						 inParticle->GetPz(),
						 inParticle->GetE()
						 )
				      );
				  
      SmearedConstituents.push_back( PseudoJet (px,py,pz,E) );
	  
      // if(inParticleS == NULL) { // Not Smeared
      //   if(inParticle->GetEta() <= 4.5 && inParticle->GetPt() >= 0.250){
      //     Int_t localCode = TMath::Abs(inParticle->GetPdgCode());
      //     // if(localCode == 2112 || localCode == 130) numNeutrals++;
      
      //     int neutralHadronAccept = 1;
      //     if((localCode == 2112 || localCode == 130)){
      //       neutralHadronAccept = 0;
      //     }
      //     //if(keepNeutrals == 1) neutralHadronAccept = 1;
      
      //     int trackAccept = 1;
      //     if(localCode > 110 && localCode != 2112 && localCode != 130 && TMath::Abs(inParticle->GetEta()) <= 3.5){
      //       // if(ranFactor > trackEff) trackAccept = 0;
      //     }
      
      //     if(neutralHadronAccept == 1 && trackAccept == 1){
      //       PseudoJet pPt(px,py,pz,E);
      //       particlesPt.push_back(pPt);
      //     }
      //   }
      // }
      
      // if(inParticleS != NULL){ // Smeared
      // 	if(inParticleS->GetEta() <= 4.5 && inParticleS->GetPt() >= 0.250){
      // 	  Int_t localCode = TMath::Abs(inParticle->GetPdgCode());
	  
      // 	  // if(localCode == 2112 || localCode == 130) numNeutrals++;
	  
      // 	  int neutralHadronAccept = 1;
      // 	  if((localCode == 2112 || localCode == 130)){
      // 	    neutralHadronAccept = 0;
      // 	  }
      // 	  //if(keepNeutrals == 1) neutralHadronAccept = 1;
	  
      // 	  int trackAccept = 1;
      // 	  if(localCode > 110 && localCode != 2112 && localCode != 130 && TMath::Abs(inParticle->GetEta()) <= 3.5){
      // 	    // if(ranFactor > trackEff) trackAccept = 0;
      // 	    // efficiency->Fill(trackAccept);
      // 	  }
	  
      // 	  if(neutralHadronAccept == 1 && trackAccept == 1){
      // 	    PseudoJet pPt(px,py,pz,E);
      // 	    pPt.set_user_index(inParticle->GetIndex());
      // 	    particlesPt.push_back(pPt);
	    
      // 	  }
      // 	}
      // }

    } // Track loop

    if ( OriginalConstituents.size() != SmearedConstituents.size() ){
      throw std::runtime_error ("OriginalConstituents.size must match SmearedConstituents.size ");
    }
    
    // constituent information
    for ( int i=0; i<OriginalConstituents.size() ; ++i ){
      auto o = OriginalConstituents.at (i);
      auto s = SmearedConstituents.at (i);
      hc_pt->Fill ( o.pt(), s.pt() );
      //      cout << o.eta() << "  " << s.eta() << endl;
      hc_eta->Fill ( o.eta(), s.eta() );
    }

    // Now analyze jets
    ClusterSequence origcs(OriginalConstituents, jet_def);
    ClusterSequence smearcs(SmearedConstituents, jet_def);

    // cout << origcs.inclusive_jets().at(0).pt() << "  " << origcs.inclusive_jets().at(0).eta() << endl;
    // cout << origcs.inclusive_jets().size() << "  "
    // 	 << select_jet_eta( origcs.inclusive_jets()).size()  << "  "
    // 	 << select_jet_pt( origcs.inclusive_jets()).size() << endl;
    vector<PseudoJet> origjets = sorted_by_pt( select_jet(origcs.inclusive_jets()) );
    vector<PseudoJet> smearedjets = sorted_by_pt( select_jet(smearcs.inclusive_jets()) );

    // Should look for fakes
    if ( origjets.size() ==0 ) continue;
    // cout << " got a jet" << endl;
    
    // Should do some matching etc. now. But keep it simple for now, just use leading
    auto origlead = origjets.at(0);
    auto SelectClose = fastjet::SelectorCircle( R );
    SelectClose.set_reference( origlead );
    auto matches = sorted_by_pt( SelectClose( smearedjets ));
    if ( matches.size() ==0 ){
      // cerr << "Lost the lead?" << endl;
      // Should do more
      continue;
    }
    auto smearedmatch=matches.at(0);


    // pt
    hjlead_pt->Fill ( origlead.pt(), smearedmatch.pt() );

    // m
    hjlead_m->Fill ( origlead.m(), smearedmatch.m() );

    // zg
    auto groomedorig = sd( origlead );
    auto groomedsmeared = sd( smearedmatch );
    if ( groomedorig == 0){
      cout << " --- Skipped. Something caused SoftDrop to return 0 ---" << endl;
      continue;
    }
    double origzg = groomedorig.structure_of<contrib::SoftDrop>().symmetry();
    if (origzg<0) origzg=0;
    double smearedzg = groomedsmeared.structure_of<contrib::SoftDrop>().symmetry();
    if (smearedzg<0) smearedzg=0;
    hjlead_zg->Fill ( origzg, smearedzg );

    // cout << origzg << "  " <<  smearedzg << endl;
    
  } // Event loop

  fout->Write();
  return 0;
  
}  
  
